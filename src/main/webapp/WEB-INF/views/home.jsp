<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Calculator</title>
	<link href="<c:url value="/resources/styles/calculator.css" />" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	
	<script>
		window.onload = function() {
			$(".key").click(function(){
		  		var key = $(this).text();
		  		
		  		$.ajax({
		  			type : "POST",
	 	            url : 'calcPress.html',
	 	            data : {value: key},
	 	            success : function(data) {
	 	                $('#screen').html(data);
	 	            }
	 	        });
			});
		}
	</script>
</head>
<body>
<div id="calculator">

	<div class="screenContainer">
		<div id="screen" >0</div>
	</div>
	
	<div class="keyContainer">
		<!-- operators and other keys -->
		<span class="key operator">+</span>
		<span class="key operator">-</span>
		<span class="key operator">/</span>
		<span class="key operator">x</span>
		<span class="key value">7</span>
		<span class="key value">8</span>
		<span class="key value">9</span>
		<span class="key clear">C</span>

		<span class="key value">4</span>
		<span class="key value">5</span>
		<span class="key value">6</span>
		<span class="key function">Fn1</span>

		<span class="key value">1</span>
		<span class="key value">2</span>
		<span class="key value">3</span>
		<span class="key function">Fn2</span>

		<span class="key value">0</span>
		<span class="key value">.</span>
		<span class="key eval">=</span>
		<span class="key function">Fn3</span>
	</div>
</div>

</body>
</html>
