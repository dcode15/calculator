package com.dougcode.calculator;

public class Calculator {

	private double total;
	private String currentValue;
	private String operator;
	
	public Calculator() {
		this.total = 0;
		this.currentValue = "";
		this.operator = "";
	}
	
	public String processKey(String key) {
		
		String result;
		
		if(isNumeric(key) || key.equals(".")) {
			result = processDigit(key);
		}
		else if (key.equals("C")) {
			reset();
			result = Double.toString(total);
		}
		else{
			result = processOperator(key);
		}
		
		return result;
	}
	
	private String processDigit(String key) {
		
		currentValue += key;
		return currentValue;
	}
	
	private String processOperator(String key) {
		
		double value;
		
		try  {  
			value = Double.parseDouble(currentValue);  
		}  
		catch(NumberFormatException nfe) {  
			value = 0;  
		} 
		
		switch(operator) {
			case "+":
				total += value;
				break;
			case "-":
				total -= value;
				break;
			case "x":
				total *= value;
				break;
			case "/":
				total /= value;
				break;
			case "":
				total = value;
				break;
			default:
				break;
		}
		
		operator = key;
		currentValue = "";
		return Double.toString(total);
	}
	
	private boolean isNumeric(String key)  
	{  
		return key.matches("-?\\d+(\\.\\d+)?");
	}
	
	public void reset() {
		this.total = 0;
		this.currentValue = "";
		this.operator = "";
	}
	
	public void setTotal(double newTotal) {
		total = newTotal;
	}
	
	public double getTotal() {
		return total;
	}
	
	public void setOperator(String newOperator) {
		operator = newOperator;
	}
	
	public String getOperator() {
		return operator;
	}
}
