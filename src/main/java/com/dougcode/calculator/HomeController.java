package com.dougcode.calculator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	private Calculator calc = new Calculator();
	
	/**
	 * Selects the home view to render
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model) {
		
		calc.reset();
		return "home";
	}
	
	@RequestMapping(value = "/calcPress", method = RequestMethod.POST)
    public @ResponseBody String handleKeyPress(@RequestParam("value") String keyValue) {
 
		String screenVal = calc.processKey(keyValue);
        return screenVal;
    }
}
